package com.max.moviecatalogservice.Resources;

import com.max.moviecatalogservice.Domain.CatalogItem;
import com.max.moviecatalogservice.Domain.Movie;
import com.max.moviecatalogservice.Domain.Rating;
import com.max.moviecatalogservice.Domain.UserRating;
import com.max.moviecatalogservice.Services.MovieInfo;
import com.max.moviecatalogservice.Services.UserRatingInfo;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.sun.prism.impl.BaseMesh;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/catalog")
public class MovieCatalogResources {
    @Autowired
    private RestTemplate restTemplate;
    //Asyncronous calls
    @Autowired
    private WebClient.Builder webClientBuilder;
    @Autowired
    private MovieInfo movieInfo;
    @Autowired
    private UserRatingInfo userRatingInfo;
    //get all rated movies
    //for each movie id call movie info service and get details
    //put them all together
    @RequestMapping("/{userId}")
    //@HystrixCommand(fallbackMethod = "getFallbackCatalog")
    public List<CatalogItem> getCatalog(@PathVariable("userId") String userID){
        //RestTemplate restTemplate = new RestTemplate();

//        List<Rating> ratings =Arrays.asList(new Rating("123",4),
//                                    new Rating("5678",5));
        //UserRating userRating = restTemplate.getForObject("http://localhost:8083/ratingsdata/users/"+userID,UserRating.class);
        UserRating userRating =userRatingInfo.getUserRating(userID);
        return userRating.getUserRating().stream().map(rating -> {
            //don't hardcore url use service discovery. -> use @Bean and @Autowire
            //for each movie id call movie info service and get details
            //Movie movie = restTemplate.getForObject("http://localhost:8082/movies/"+rating.getMovieId(), Movie.class);
            Movie movie = movieInfo.getMovieInfo(rating.getMovieId());
//            Movie movie = webClientBuilder.build()
//                    .get()
//                    .uri("http://localhost:8082/movies/"+rating.getMovieId())
//                    .retrieve()
//                    //mono is like promise in asynchronous
//                    .bodyToMono(Movie.class)
//                    .block();
        // put them all together
        return new CatalogItem(movie.getName(), "action", rating.getRating());
        }).collect(Collectors.toList());

        //return Collections.singletonList(new CatalogItem("Transformers","action",4));
    }
//    //Granual level of fallback
//    @HystrixCommand(fallbackMethod = "getFallbackUserRating")
//    public UserRating getUserRating (String userID){
//        return restTemplate.getForObject("http://rating-data-service/ratingsdata/users/"+userID,UserRating.class);
//    }
//    @HystrixCommand(fallbackMethod = "getFallbackMovieInfo")
//    public Movie getMovieInfo(String movieId){
//        return  restTemplate.getForObject("http://movie-info-service/movies/"+movieId, Movie.class);
//    }
//    public UserRating getFallbackUserRating (String userID){
//        UserRating userRating = new UserRating();
//        userRating.setUserRating(Arrays.asList(new Rating("",0)));
//        userRating.setUserId(userID);
//        return userRating;
//    }
//    public Movie getFallbackMovieInfo(String movieId){
//        Movie movie = new Movie(movieId,"NOT FOUND");
//        return movie;
//    }
//    public List<CatalogItem> getFallbackCatalog(@PathVariable("userId") String userID){
//        return Arrays.asList(new CatalogItem("","",0));
//    }

}
