package com.max.moviecatalogservice.Services;

import com.max.moviecatalogservice.Domain.Movie;
import com.max.moviecatalogservice.Domain.Rating;
import com.max.moviecatalogservice.Domain.UserRating;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
@Service
public class MovieInfo {
    @Autowired
    private RestTemplate restTemplate;
    //Granual level of fallback

    @HystrixCommand(fallbackMethod = "getFallbackMovieInfo")
    public Movie getMovieInfo(String movieId){
        return  restTemplate.getForObject("http://movie-info-service/movies/"+movieId, Movie.class);
    }

    public Movie getFallbackMovieInfo(String movieId){
        Movie movie = new Movie(movieId,"NOT FOUND");
        return movie;
    }
}
