package com.max.moviecatalogservice.Services;

import com.max.moviecatalogservice.Domain.Rating;
import com.max.moviecatalogservice.Domain.UserRating;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

@Service
public class UserRatingInfo {
    @Autowired
    private RestTemplate restTemplate;
    @HystrixCommand(fallbackMethod = "getFallbackUserRating")
    public UserRating getUserRating (String userID){
        return restTemplate.getForObject("http://rating-data-service/ratingsdata/users/"+userID,UserRating.class);
    }

    public UserRating getFallbackUserRating (String userID){
        UserRating userRating = new UserRating();
        userRating.setUserRating(Arrays.asList(new Rating("",0)));
        userRating.setUserId(userID);
        return userRating;
    }
}
